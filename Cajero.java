public class Cajero extends Persona {
    private String cuenta;

    public String getCuenta() {
        return this.cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public Cajero() {
        this.cuenta = "#123 123 123 123";
    }
}
