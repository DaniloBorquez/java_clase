public class Cliente extends Persona {

    private String email;

    public Cliente() {
        this.nombre = "Pepito";
        this.rut = "1-9";
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
