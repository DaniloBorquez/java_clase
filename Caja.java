public class Caja {
    private Cajero cajero;
    private int ID = 0;

    private static int contador = 0;

    public Caja() {
        ID = Caja.contador++;
    }

    public Cajero getCajero() {
        return this.cajero;
    }

    public void setCajero(Cajero cajero) {
        this.cajero = cajero;
    }

    public int getID() {
        return this.ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String toString() {
        return "El ID de la caja es: " + String.valueOf(ID);
    }
}
