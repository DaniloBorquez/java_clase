public class Main {
    static public void main(String[] args) {
        Cliente c1 = new Cliente();

        System.out.println("nombre de cliente: " + c1.getNombre());
        System.out.println("Rut de cliente: " + c1.getRut());

        System.out.println("nombre de cliente: " + c1.nombre);
        System.out.println("Rut de cliente: " + c1.rut);

        c1.setNombre("Juanito");
        System.out.println("nombre de cliente: " + c1.getNombre());

        Caja ca1 = new Caja();
        Caja ca2 = new Caja();
        Caja ca3 = new Caja();
        Caja ca4 = new Caja();
        Caja ca5 = new Caja();

        System.out.println("Identificador caja: " + ca1);
        System.out.println("Identificador caja: " + ca2);
        System.out.println("Identificador caja: " + ca3);
        System.out.println("Identificador caja: " + ca4);
        System.out.println("Identificador caja: " + ca5);
        System.out.println("Identificador caja: " + ca1);
        System.out.println("Caja: " + ca1);
    }
}
